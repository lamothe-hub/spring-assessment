package com.citi.training.assessment.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.assessment.dao.mysql.MysqlTradeDao;
import com.citi.training.assessment.model.Trade;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {
	


	@Autowired 
	MysqlTradeDao mysqlTradeDao; 
	
	@Test
	public void test_createAndFindById() {
		int newId = mysqlTradeDao.createTrade(new Trade(1, "TestAAPL", 90.0,400));
		assertNotNull(mysqlTradeDao.retrieveTrade(newId)); 
	}
	
	@Test
	public void test_createAndDeleteById() {
		int newId = mysqlTradeDao.createTrade(new Trade(2, "TestMSFT", 90.0,400));
		mysqlTradeDao.deleteById(newId);
		assertNull(mysqlTradeDao.retrieveTrade(newId));
	}

}
