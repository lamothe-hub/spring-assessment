package com.citi.training.assessment.model;

import static org.junit.Assert.*;

import org.junit.Test;


public class TradeTests {

	@Test
	public void test() {
		Trade testTrade = new Trade(1, "AAPL", 87.0, 400);
		assert(testTrade.getId() == 1);
		assert(testTrade.getStock().equals("AAPL"));
		assert(testTrade.getPrice() == 87.0);
		assert(testTrade.getVolume() == 400);
	}
	
	@Test
    public void test_Trade_setters() {
        Trade testTrade = new Trade(1, "AAPL", 87, 400);
        testTrade.setId(10);
        testTrade.setStock("MSFT");
        testTrade.setPrice(100.00);
        testTrade.setVolume(3000);


        assert(testTrade.getId() == 10);
        assert(testTrade.getStock().equals("MSFT"));
        assert(testTrade.getPrice() == 100.00);
        assert(testTrade.getVolume() == 3000);
    }

}
