package com.citi.training.assessment.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.rest.TradeController;
import com.citi.training.assessment.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest
public class TradeControllerTests {
    private static final Logger logger =
            LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private TradeService mockTradeService;
    
    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade(1, "AAPL", 29.99, 100);

        when(mockTradeService
                .create(any(Trade.class)))
            .thenReturn(testTrade.getId());

        this.mockMvc.perform(
                post(TradeController.BASE_PATH)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTrade.getId()))
                .andReturn();
        logger.info("Result from Create Product");
    }
    
    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete(TradeController.BASE_PATH + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from productDao.delete: "
                    + result.getResponse().getContentAsString());
    }

    
}
