package com.citi.training.assessment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@RunWith(SpringRunner.class) 
@SpringBootTest 
public class TradeServiceTests {

	@Autowired
	private TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_createTrade() {
		
		int newId = 1;
 		Trade testProduct = new Trade(99, "AAPL", 88.8, 200);
		when(mockTradeDao.createTrade(any(Trade.class))).thenReturn(newId);
		int createdId = tradeService.create(testProduct);
		assertEquals(newId, createdId);
	
	}
	@Test
	public void test_deleteById() {
		
		tradeService.deleteById(1);
		verify(mockTradeDao).deleteById(1);
	
	}
	
	@Test
	public void test_findById() {
		
		int testId = 66;
		Trade testTrade = new Trade(66, "AAPL", 100.99, 300);
		
		when(mockTradeDao.retrieveTrade(testId)).thenReturn(testTrade);
		
		Trade returnedTrade = tradeService.retrieveTrade(testId);
		
		verify(mockTradeDao).retrieveTrade(testId);
		
		assertEquals(testTrade, returnedTrade);	
	
	}

}
