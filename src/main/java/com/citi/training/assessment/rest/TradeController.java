package com.citi.training.assessment.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.service.TradeService;



@RestController
@RequestMapping(TradeController.BASE_PATH)
public class TradeController {
	
    public static final String BASE_PATH = "/trade";
    private static final Logger logger = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    TradeService tradeService;

	@RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public Trade retrieveTrade(@PathVariable int id) {
		logger.info("Controller has received a retrieveTrade request with id: " + id);
		return tradeService.retrieveTrade(id);
	
	}
	
    @RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(@RequestBody Trade product) {
		product.setId(tradeService.create(product));
		return new ResponseEntity<Trade>(product, HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        tradeService.deleteById(id);
    }

	
}
