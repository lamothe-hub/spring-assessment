package com.citi.training.assessment.dao;

import com.citi.training.assessment.model.Trade;

public interface TradeDao {
	Trade retrieveTrade(int id);
	int createTrade(Trade trade);
	public void deleteById(int id);
}
