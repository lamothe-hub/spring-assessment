package com.citi.training.assessment.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;


@Component
public class MysqlTradeDao implements TradeDao {
    private static final Logger logger = LoggerFactory.getLogger(TradeDao.class);

	@Autowired
    JdbcTemplate tpl;
	
	@Override
	public Trade retrieveTrade(int id) {
		logger.debug("retrieveTrade() called in mysql dao");
		List<Trade> trade;
		try {
			trade = this.tpl.query(
					"select id, stock, price, volume from trade where id = ?",
					new Object[] {id}, 
					new TradeMapper()
				);
		} catch(Exception ex){
			logger.error("retrieveTrade query has failed. There could be a database connection or schema issue.");
			return null;
		}
		try {
			return trade.get(0);
		} catch(Exception ex) {
			logger.warn("There is no entry with the requested id of " + id);
			return null;
		}		
	}
	
	@Override
	public int createTrade(Trade trade) {
		logger.debug("creatTrade called in mysql dao");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
        	this.tpl.update(
                    new PreparedStatementCreator() {
                        @Override
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            PreparedStatement ps =
                                    connection.prepareStatement("insert into trade (stock, price, volume) values (?, ?, ?)",
                                    Statement.RETURN_GENERATED_KEYS);
                            ps.setString(1, trade.getStock());
                            ps.setDouble(2, trade.getPrice());
                            ps.setInt(3,  trade.getVolume());
                            return ps;
                        }
                    },
                    keyHolder);
        	return keyHolder.getKey().intValue();
        } catch(Exception ex) {
        	logger.warn("Could not add the Trade to the database.");
        	return -1;
        }
        
	}
	
	public void deleteById(int id) {
		try {
			this.tpl.update("delete from trade where id=?", id);
	        logger.info("deleted trade with id: " + id);
		} catch(Exception ex) {
			logger.warn("Could not delete the trade with id: " + id);
		}
        
	}
	
	private static final class TradeMapper implements RowMapper<Trade> {
        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Trade(rs.getInt("id"),
                                rs.getString("stock"),
                                rs.getDouble("price"),
                                rs.getInt("volume"));
        }
    }
	

}
