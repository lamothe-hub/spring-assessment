package com.citi.training.assessment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@Component
public class TradeService {
	
	@Autowired 
	TradeDao tradeDao;
	
	public Trade retrieveTrade(int id) {
		return tradeDao.retrieveTrade(id);
	}

	public int create(Trade trade) {
		return tradeDao.createTrade(trade);
	}
	
	public void deleteById(int id) {
		tradeDao.deleteById(id);
	}
}