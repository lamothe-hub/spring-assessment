CREATE TABLE trade (
 id INTEGER NOT NULL AUTO_INCREMENT,
 stock VARCHAR(50) NOT NULL,
 price DOUBLE NOT NULL,
 volume INT NOT NULL,
 PRIMARY KEY (id)
);
